#include <windows.h>
#include <stdio.h>
#include <string>

#include "detours.h"

#pragma comment(lib, "detours.lib")

// Don't fear DWORDs, they're just 32bit unsigned integers.

typedef void(__cdecl* gOpenImageLink)(const char* SrcBuf);
gOpenImageLink OpenImageLink = NULL;

void toClipboard(const std::string &s) {
	OpenClipboard(0);
	EmptyClipboard();
	HGLOBAL hg = GlobalAlloc(GMEM_MOVEABLE, s.size() + 1);

	if (!hg) {
		CloseClipboard();
		return;
	}

	memcpy(GlobalLock(hg), s.c_str(), s.size() + 1);
	GlobalUnlock(hg);
	SetClipboardData(CF_TEXT, hg);
	CloseClipboard();
	GlobalFree(hg);
}

void hkOpenImageLink(const char* SrcBuf) {
	__asm pushad;

	char newSrcBuf[1024];
	sprintf_s(newSrcBuf, "%s.png", SrcBuf);
	toClipboard(newSrcBuf);
	Beep(500, 500);
	exit(0);

	__asm popad;

	return OpenImageLink(SrcBuf);
}

DWORD FindPattern(DWORD dwStart, DWORD dwLen, BYTE* pszPatt, char pszMask[]) {
	unsigned int i = NULL;
	int iLen = strlen(pszMask) - 1;

	for (DWORD dwRet = dwStart; dwRet < dwStart + dwLen; dwRet++) {
		// Oh god
		if (*(BYTE*)dwRet == pszPatt[i] || pszMask[i] == '?') {
			if (pszMask[i + 1] == '\0')
				return dwRet - iLen;
			i++;
		}
		else
			i = NULL;
	}
	return NULL;
}

int __stdcall DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpvReserved) {
	if (dwReason == DLL_PROCESS_ATTACH) {
		//OpenImageLink = (gOpenImageLink)DetourFunction((PBYTE)0xA72478, (PBYTE)hkOpenImageLink);

		DWORD hGyazo = NULL;
		for (; hGyazo == NULL; Sleep(100))
			hGyazo = (DWORD)GetModuleHandle("OriginalGyazowin.exe");

		DWORD Addr = FindPattern(hGyazo, 0x01121000, (PBYTE)"\x55\x8B\xEC\x8B\x45\x00\x83\xEC\x7C\x8D\x48\x00", "xxxxx?xxxxx?");
		OpenImageLink = (gOpenImageLink)DetourFunction((PBYTE)Addr, (PBYTE)hkOpenImageLink);
	}

	return 1;
}