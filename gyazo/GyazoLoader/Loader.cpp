#include <Windows.h>
#include <stdio.h>
#include <string>
#include <TlHelp32.h>

DWORD GetProcessIdByName(char* procName)
{
	HANDLE SnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (SnapShot == INVALID_HANDLE_VALUE)
		return 0;

	PROCESSENTRY32 procEntry;
	procEntry.dwSize = sizeof(PROCESSENTRY32);

	if (!Process32First(SnapShot, &procEntry))
		return 0;

	while (Process32Next(SnapShot, &procEntry))
	{
		if (!_strcmpi(procEntry.szExeFile, procName))
		{
			return procEntry.th32ProcessID;
		}
	}

	CloseHandle(SnapShot);
	return -1;
}

void WaitForProcessToAppear(const char* procName, DWORD dwSleep)
{
	bool bAppeared = false;

	while (!bAppeared)
	{
		HANDLE SnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

		if (SnapShot == INVALID_HANDLE_VALUE)
			return;

		PROCESSENTRY32 procEntry;
		procEntry.dwSize = sizeof(PROCESSENTRY32);

		if (!Process32First(SnapShot, &procEntry))
			return;

		while (Process32Next(SnapShot, &procEntry) && !bAppeared)
		{
			if (!_strcmpi(procEntry.szExeFile, procName))
			{
				bAppeared = true;
			}
		}

		CloseHandle(SnapShot);
		Sleep(dwSleep);
	}
}

bool InjectGyazo()
{
	DWORD dwPID = GetProcessIdByName("OriginalGyazowin.exe");
	char dir[1024], gyazoPath[1024];
	GetCurrentDirectory(1024, dir);
	sprintf_s(gyazoPath, "%s\\Gyazowin.dll", dir);
	LPCWSTR szDLLPath = (LPCWSTR)gyazoPath;

	int cszDLL;
	LPVOID lpAddress;
	HMODULE hMod;
	HANDLE hThread;
	HANDLE hProcess = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ, FALSE, dwPID);

	if (hProcess == NULL)
		return 0;

	cszDLL = (wcslen(szDLLPath) + 1) * sizeof(WCHAR);

	lpAddress = VirtualAllocEx(hProcess, NULL, cszDLL, MEM_COMMIT, PAGE_EXECUTE_READWRITE);

	if (lpAddress == NULL)
		return 0;

	WriteProcessMemory(hProcess, lpAddress, szDLLPath, cszDLL, NULL);

	hMod = GetModuleHandle("kernel32.dll");

	if (hMod == NULL)
		return 0;

	hThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)(GetProcAddress(hMod, "LoadLibraryA")), lpAddress, 0, NULL);

	if (hThread != 0)
	{
		WaitForSingleObject(hThread, INFINITE);
		VirtualFreeEx(hProcess, lpAddress, 0, MEM_RELEASE);
		CloseHandle(hThread);
	}

	return 1;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	PROCESS_INFORMATION pi = {};
	STARTUPINFO si = {};
	ZeroMemory(&pi, sizeof(pi));
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(STARTUPINFO);

	char dir[1024], gyazoPath[1024];
	GetCurrentDirectory(1024, dir);
	sprintf_s(gyazoPath, "%s\\OriginalGyazowin.exe", dir);
	CreateProcess(gyazoPath, 0, 0, 0, 0, 0, 0, 0, &si, &pi);
	WaitForProcessToAppear("OriginalGyazowin.exe", 100);
	InjectGyazo();
}